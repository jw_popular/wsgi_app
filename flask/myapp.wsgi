import sys
import os

#app's path
#sys.path.insert(0,"D:/all_python/python_pi/reference/modules/wsgi_app")

sys.path.insert(0, os.path.dirname(__file__))


#Initialize WSGI app object
from myapp import app as application
