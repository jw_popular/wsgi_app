import sys
import os

#app's path
#sys.path.insert(0,"D:/all_python/python_pi/reference/modules/wsgi_app")

sys.path.insert(0, os.path.dirname(__file__))

from test import app

#Initialize WSGI app object
application = app