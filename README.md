

https://www.maxlist.xyz/2019/11/10/flask-sqlalchemy-setting/
https://ithelp.ithome.com.tw/articles/10271872
https://github.com/hsuanchi/flask-template
https://www.maxlist.xyz/2020/08/06/flask-application-factories/
# main.py
 
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
 
db = SQLAlchemy()
 
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:////tmp/test.db"
 
db.init_app(app)
 
@app.route('/create_db')
def index():
    db.create_all()
    return 'ok'
 
if __name__ == "__main__":
    app.run()



